---
title: "Contact"
type: post
date: 2024-12-25T17:13:05-05:00
url: /contact/
image: /images/2022-thumbs/contact.png
categories:
  - News
tags:
  - about
draft: true
---

So you wanna talk with me eh? Ok fine here's how to get in touch.

## Communities
*A lot of these are community links.*  
[Blog](https://10leej.com): I shout at things here. In fact your already here, so why not stay?  
[YouTube](https://youtube.com/@10leej): I primarily post videos and streams here.  
[PeerTube](https://videos.danksquad.org/c/joshua_lee/videos?languageOneOf=en&s=2): I sync the YouTube channel to here, sometimes I might exclusive content here as well.  
[Mastodon](https://fosstodon.org/@10leej): I'm a nerd here. *- you can follow from [Mastodon](https://joinmastodon.org/) and any [ActivityPub](https://www.w3.org/TR/activitypub/) enabled instance*  
[Discord](https://discord.com/invite/cUfbCBF): I'm a pretty chill dude here.  
[Matrix](https://matrix.to/#/#10leejs-distrohackers:matrix.org): I make developers angry here. Direct is @10leej:matrix.org   
[X (formerly Twitter)](https://twitter.com/10leej): I get in political fights here.  
[reddit](https://www.reddit.com/r/10leej/): I don't even know what to do here.  

## Direct
[Email](mailto:josh@10leej.com) my email

### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
