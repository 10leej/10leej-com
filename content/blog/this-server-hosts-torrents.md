---
title: "This Server Hosts Torrents"
type: post
date: 2025-02-15T15:16:58-05:00
url: /this-server-hosts-torrents/
image: /images/2022-thumbs/this-server-hosts-torrents.png
categories:
  - Blog
tags:
  - Torrenting
draft: true
---

## Did You Know?

This server has been running tranmission with a locked webgui for a while now?

![image-showing-transmission-interface](/images/2025/this-server-hosts-torrents.png)

## Why?
Well this server has been up and running for.... 5 years now initially starting as a webhost just for this blog and eventually hosting what I assume to be is the most popular [Planarally](https://planarally.io) instance. Which while I don't rally use it much myself anymore it's cool that there are so many people that still use it. It's also just a wy to give back to some of myu favorite projects in helping them seed torrents.

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
