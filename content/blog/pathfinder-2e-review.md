---
title: "Pathfinder 2e Review"
type: post
date: 2025-02-15T15:00:02-05:00
url: /pathfinder-2e-review/
image: /images/2025-thumbs/pathfinder-2e-review.png
categories:
  - RPGS
tags:
  - PF2
draft: true
---

# Pathfinder 2e Remastered

So I'm now played 10 sessions with Pathfinder 2nd Editions Remaster and I think it's time I shared some thoughts on the system coming from D&D's 5th edition.

## Players
### Character Creation
### Combat
### Levelling UP

## GMs
### Encounter Planning
### Combat
### Out of Combat Encounters

## Conclusions

#### Support Me
[Amazon Wish List](https://www.amazon.com/hz/wishlist/ls/2QSOOHKDIZX2X?ref_=wl_share)  
[Librepay](https://liberapay.com/10leej/)

*I do not accept any cryptocurrencies at this time*

> I do not take sponsors unless they agree to my freedom of speech and thought about their products, if your product sucks I will in fact to you so before hand. All donations are non-refundable.
